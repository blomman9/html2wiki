if (typeof Vue === "undefined") {
    throw "Vue.js library not installed";
}

var app = new Vue({
    el: '#app',
    data: {
        title: 'Convert HTML to Wiki',
        textFrom: '',
        textTo: '',
        pandocTakeover: false,
        selectedFrom: 'HTML',
        selectedTo: 'DokuWiki',
        firstTitle: '',
        foundImages: [],
        settings: [],
        isAnimating: false,
        optionsFrom: [
            { text: 'HTML', value: 'HTML' }
        ],
        optionsTo: [
            { text: 'Markdown', value: 'Markdown' },
            { text: 'DokuWiki', value: 'DokuWiki', notoc: true, keepstyles: false, keepscripts: false }
        ]
    },
    methods: {
        copyText: function (id) {
            let range;
            const elem = document.getElementById(id);

            if (document.selection) {
                range = document.body.createTextRange();
                range.moveToElementText(elem);
                range.select().createTextRange();
            }
            else {
                range = document.createRange();
                range.setStartBefore(elem);
                range.setEndAfter(elem);
                window.getSelection().addRange(range);
            }
            var successful;

            try {
                if (document.queryCommandEnabled("copy")) {
                    successful = document.execCommand('copy');
                } else if (navigator && navigator.clipboard) {
                    successful = navigator.clipboard.writeText(range.toString())
                }
                var msg = successful ? 'successful' : 'unsuccessful';
                console.info('Copy was ' + msg);
            } catch (err) {
                console.info('Oops, unable to copy');
            }

            if (successful) {
                // Blink the container
                elem.className.add('blick')
                //elem.className = elem.className + " blink";
                window.setTimeout(function () {
                    //elem.className = elem.className.remove(/\bblink\b/g, "");
                    elem.className.remove('blick')
                }, 200);
            }

            // Show notification
            let notification = document.createElement("div");
            notification.className = "notification " + (successful ? 'is-primary' : 'is-danger');

            let button = document.createElement("button");
            button.className = "delete";
            notification.appendChild(button);

            var text = successful ? 'Copied text' : 'Could not copy text';
            var textnode = document.createTextNode(text);
            notification.appendChild(textnode);

            const wrapper = document.getElementById('notifications');
            wrapper.appendChild(notification);

            window.setTimeout(function () {
                if (notification.parentNode) notification.parentNode.removeChild(notification);
            }, 2500);

            (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
                $notification = $delete.parentNode;

                $delete.addEventListener('click', () => {
                    $notification.parentNode.removeChild($notification);
                });
            });

            window.getSelection().removeAllRanges();
        },
        updateSettings: function () {
            if (this.selectedTo === 'Markdown') {
                this.settings = [];
            }
            if (this.selectedTo === 'Pandoc') {
                this.settings = [];
            }
            if (this.selectedTo === 'DokuWiki') {
                this.settings = [
                    { text: "Notoc", title: "notoc", value: this.optionsTo[1].notoc },
                    { text: "Keep Styles", title: "keepstyles", value: this.optionsTo[1].keepstyles },
                    { text: "Keep Scripts", title: "keepscripts", value: this.optionsTo[1].keepscripts }
                ]
            } else {

            }
        },
        change: function () {
            if (this.isAnimating) {
                return;
            }
            this.isAnimating = true;
            setTimeout(this.changeEmit, 2000);
        },
        changeEmit: function () {
            this.isAnimating = false;
            this.submit();
        },
        submit: async function (e) {
            this.textTo = '';
            this.firstTitle = '';
            this.foundImages = [];

            if (this.selectedTo === 'Markdown') {
                var turndownService = new TurndownService()
                this.textTo = turndownService.turndown(this.textFrom)
            }

            else if (this.selectedTo === 'DokuWiki') {
                var turndokuService = new TurndokuService();
                turndokuService.resetImages();
                var extras = '';
                if (this.selectedTo === 'DokuWiki') {
                    extras += "\n\n" + '~~NOTOC~~';
                }
                this.textTo = turndokuService.turndoku(this.textFrom) + extras;
                this.foundImages = turndokuService.getImages();
            }

            else if (this.pandocTakeover) {
                var pandocService = new PandocService();
                pandocService.reset();
                this.textTo = await pandocService.pandoc(this.selectedFrom, this.selectedTo, this.textFrom);
                this.firstTitle = pandocService.getFirstTitle();
                this.foundImages = pandocService.getImages();
            }

            this.updateSettings();
        }
    },
    async mounted() {
        const el = document.getElementById('app');
        const urlParams = new URLSearchParams(window.location.search);
        const inputText = urlParams.get('input');
        const defaultPandocFrom = 'html';
        const defaultPandocTo = 'dokuwiki';
        var list, str, val, i;

        var pandocService = new PandocService();
        if (await pandocService.supported()) {
            this.title = 'Pandoc Converter';
            this.pandocTakeover = true;
            pandocService.getFrom().then((pandocFrom) => {
                list = [];
                for (i = 0; i < pandocFrom.length; i++) {
                    val = pandocFrom[i];
                    if (val == defaultPandocFrom) {
                        this.selectedFrom = val;
                    }
                    str = val.replace('_', '');
                    str = val.charAt(0).toUpperCase() + val.slice(1);
                    list.push({
                        text: str,
                        value: val
                    });
                }
                this.optionsFrom = list;
            })
            pandocService.getTo().then((pandocTo) => {
                list = [];
                for (i = 0; i < pandocTo.length; i++) {
                    val = pandocTo[i];
                    if (val == defaultPandocTo) {
                        this.selectedTo = val;
                    }
                    str = val.replace('_', '');
                    str = val.charAt(0).toUpperCase() + val.slice(1);
                    list.push({
                        text: str,
                        value: val
                    });
                }
                this.optionsTo = list;
            })
        }
        if (inputText) {
            const htmltext = document.getElementById('html-to-wiki-text');
            this.textFrom = inputText;
            this.submit();
        }
        el.classList.remove('is-loading');
        this.updateSettings();
    }
});
