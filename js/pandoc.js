var PandocService = (function () {
    'use strict';

    function extend(destination) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];
            for (var key in source) {
                if (source.hasOwnProperty(key)) destination[key] = source[key];
            }
        }
        return destination
    }

    var firstTitle = '';

    var foundImages = [];

    function PandocService(options) {
        if (!(this instanceof PandocService)) return new PandocService(options)

        var defaults = {
            br: '  '
        };
        this.options = extend({}, defaults, options);
    }

    PandocService.prototype = {
        /**
         * The entry point for converting a string or DOM node to Markdown
         * @public
         * @param {String|HTMLElement} input The string or DOM node to convert
         * @returns A Markdown representation of the input
         * @type String
         */

        pandoc: async function (from, to, input) {
            if (!canConvert(input)) {
                throw new TypeError(
                    input + ' is not a string, or an element/document/fragment node.'
                )
            }

            if (input === '') return ''

            try {
                var request = new Request(`/pandoc/?action=convert&from=${from}&to=${to}`, {
                    method: 'POST',
                    body: input,
                    headers: new Headers()
                });
                let response = await fetch(request);
                let data = await response.json();
                if (data.success) {
                    if (typeof data.first_title === 'string') {
                        firstTitle = data.first_title;
                    }
                    if (typeof data.images === 'object') {
                        foundImages = data.images;
                    }
                    if (typeof data.result === 'object') {
                        return data.result.join("\n");
                    }
                    return data.result;
                }
            } catch (error) {
                console.debug(error);
            }

            return '';
        },

        supported: async function () {
            return fetch('/pandoc/?action=pandoc-supported').then(response => response.json()).then((data) => {
                return data.success === true;
            }).catch(() => {
                return false;
            });
        },

        getPeriferals: async function (type) {
            if (!(type == 'input' || type == 'output')) {
                return false;
            }
            return fetch(`/pandoc/?action=list-${type}-formats`).then(response => response.json()).then(data => {
                return (data.success) ? data.result : false;
            }).catch((error) => {
                console.debug(error);
            })
        },

        getFrom: async function () {
            return this.getPeriferals('input');
        },

        getTo: async function () {
            return this.getPeriferals('output');
        },

        getFirstTitle: function () {
            return firstTitle;
        },

        getImages: function () {
            return foundImages;
        },

        reset: function () {
            firstTitle = '';
            foundImages = [];
        },
    };

    /**
     * Determines whether an input can be converted
     * @private
     * @param {String|HTMLElement} input Describe this parameter
     * @returns Describe what it returns
     * @type String|Object|Array|Boolean|Number
     */

    function canConvert(input) {
        return (
            input != null && (
                typeof input === 'string' ||
                (input.nodeType && (
                    input.nodeType === 1 || input.nodeType === 9 || input.nodeType === 11
                ))
            )
        )
    }

    return PandocService;

}());